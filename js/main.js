$(".btn-primary").on("click", handleButtonClick);

function jsonFlickrFeed(json) {
  
  $.each(json.items, function(i, item) {
    $("<img />").attr("src", item.media.m).appendTo("#images");
    return i<6;
  });
}
function handleButtonClick() {
  var tag = $("#inputText").val() || 'web';
  
  $.ajax({
    url: 'https://api.flickr.com/services/feeds/photos_public.gne',
    dataType: 'jsonp',
    data: {  "tags": tag, "page": "1","per_page": "1","format": "json"}
  });
}